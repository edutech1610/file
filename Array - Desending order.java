import java.util.stream.IntStream;

public class Desending {
    public static void main(String args[]) {

       int arr[] = {0,1,2,4,2,1};

       System.out.println(isDesending(arr));

    }


    public static boolean isDesending(int[] a)
    {
        // base case
        if (a == null || a.length <= 1) {
            return true;
        }
 
        return IntStream.range(0, a.length - 1).noneMatch(i -> a[i] < a[i + 1]);
    }

}
